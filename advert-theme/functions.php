<?php

// Theme styles and scripts
function load_theme_scripts() {
    wp_enqueue_style( 'style-main', get_stylesheet_uri() );
    wp_enqueue_script( 'script-main', get_template_directory_uri() . '/js/main.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'load_theme_scripts' );

// BOOTSTRAP CSS
function enqueue_bootstrap_styles(){
    wp_enqueue_style('bootstrap_css', 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css');
}
add_action( 'wp_enqueue_scripts', 'enqueue_bootstrap_styles' );

// BOOTSTRAP JAVASCRIPT
function enqueue_bootstrap_scripts() {
    wp_enqueue_script( 'bootstrap_jquery', 'https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js', array(), '3.5.1', true );
    wp_enqueue_script( 'bootstrap_popper', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js', array(), '1.16.1', true );
    wp_enqueue_script( 'bootstrap_javascript', 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js', array(), '4.6.2', true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_bootstrap_scripts' );

// Register navigation
register_nav_menus(
    array(
        'menu-1' => esc_html__( 'Primary', 'advert-theme' ),
    )
);