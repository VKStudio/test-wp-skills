<?php get_header(); ?>

<div id="primary" class="content-area container">
    <main id="main" class="site-main row" role="main">
        <?php
        while ( have_posts() ) : the_post(); ?>
            <h1><?php the_title(); ?></h1><br>
            <p><?php the_content(); ?></p>
        <?php
        endwhile;
        ?>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
