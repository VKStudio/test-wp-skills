jQuery(function ($) {
    // Select/Upload image(s) event
    $('body').on('click', '.imb_upload_image_button', function (e) {
        e.preventDefault();

        let button = $(this),
            custom_uploader = wp.media({
                title: 'Выбери или загрузи изображение',
                library: {
                    type: 'image'
                },
                button: {
                    text: 'Загрузить'
                },
                multiple: false
            }).on('select', function () {
                let attachment = custom_uploader.state().get('selection').first().toJSON();
                $(button).removeClass('button').html('<img class="true_pre_image" src="' + attachment.url + '" style="max-width:100%;display:block;" />').next().val(attachment.id).next().show();
            })
                .open();
    });

    // Remove image event
    $('body').on('click', '.imb_remove_image_button', function () {
        $(this).hide().prev().val('').prev().addClass('button').html('Загрузить изображение');
        return false;
    });

});
