jQuery(document).ready(function($){
    $('form.ajax').on('submit', function(e){
        e.preventDefault();
        $(".error_msg").css("display","none");
        $(".success_msg").css("display","none");
        $(".progress-spinner").css("display","flex");
        let email = $('.form-email').val(),
            header = $('.form-header').val(),
            atthFile = jQuery(document).find('input[type="file"]'),
            individualFile = atthFile[0].files[0];

        let formData = new FormData();
        formData.append('action', 'set_form');
        formData.append('nonce_code', wpAjax.nonce);
        formData.append('email', email);
        formData.append('header', header);
        formData.append('file', individualFile);

        $.ajax({
            type: 'POST',
            url: wpAjax.ajaxUrl,
            data: formData,
            contentType: false,
            processData: false,
            success: function(response){
                $(".progress-spinner").css("display","none");
                $(".success_msg").css("display","block");
            },
            error: function(data){
                $(".progress-spinner").css("display","none");
                $(".error_msg").css("display","block");
            }
        });
        $('.ajax')[0].reset();
    });

    // File form validator
    $(".form-file").change(function () {
        $(".error_msg").css("display","none");
        var fileExtension = ['jpeg', 'jpg', 'png'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            $(".error_msg").text("Не верный формат изображения! Мы поддерживает только: "+fileExtension.join(', '));
            $(".error_msg").css("display","block");
            $(this).val('');
        }
    });
});