<?php

define('META_KEY_IMG', 'imb_image');
define('TIME_OUT_SEND_EMAIL', 120);

/* Enqueue scripts and style from parent theme */
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );

function enqueue_parent_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

// Custom scripts
add_action( 'wp_enqueue_scripts', 'load_custom_scripts' );
add_action( 'admin_enqueue_scripts', 'load_custom_scripts' );

function load_custom_scripts() {
    wp_enqueue_script( 'script-meta-box-image', get_theme_file_uri() . '/js/meta-box-image.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'script-ajax-form', get_theme_file_uri() . '/js/ajax-form.js', array('jquery'), '1.0.0', true );
}

// Path to AJAX wp file for front-end
add_action( 'wp_enqueue_scripts', 'wp_ajax_data', 99 );

function wp_ajax_data(){
    wp_localize_script( 'script-main', 'wpAjax',
        array(
            'ajaxUrl' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce('ajax-nonce')
        )
    );
}

/* Custom post type for ADVERTS */
include dirname( __FILE__ ) . '/inc/custom-posts-types.php';

// Custom meta box for ADVERTS
include dirname( __FILE__ ) . '/inc/meta-box-image.php';

// Forms
include dirname( __FILE__ ) . '/inc/forms.php';
