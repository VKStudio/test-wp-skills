const path = require('path');
const miniCss = require('mini-css-extract-plugin');
const CssMinimizer = require("css-minimizer-webpack-plugin");

module.exports = {
    mode: 'production',
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'assets'),
        filename: 'scripts.js'
    },
    module: {
        rules: [
            {
                test: /\.s?[c]ss$/i,
                use: [
                    miniCss.loader,
                    {
                        loader : 'css-loader',
                        options: { url : false }
                    },
                ],
            },
        ],
    },
    optimization: {
        minimizer: [
            new CssMinimizer(),
        ],
    },
    plugins: [
        new miniCss({
            filename: '../style.css',
        }),
    ]
};