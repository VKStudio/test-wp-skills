<?php
/* Template Name: Add new adverts post */

get_header();


?>
    <div class="container p-5">
        <form action="" method="post" class="ajax" enctype="multipart/form-data">
            <div class="form-group row">
                <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" name="email" class="form-control form-email" id="inputEmail" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="textarea" class="col-sm-2 col-form-label">Заголовок</label>
                <div class="col-sm-10">
                    <textarea class="form-control form-header" name="header" id="textarea" rows="3" required></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="uploadFile" class="col-sm-2 col-form-label">Изображение</label>
                <div class="col-sm-10">
                    <input type="file" accept="image/*" name="atth-file" class="form-control-file form-file" id="uploadFile" required>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary submitbtn">Опубликовать</button>
                </div>
            </div>
            <div class="success_msg alert alert-success" role="alert" style="display: none">
                Объявление добавлено! Спасибо
            </div>
            <div class="error_msg alert alert-warning" role="alert" style="display: none">
                Что-то пошло не так =(
            </div>
        </form>
        <div class="progress-spinner">
            <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
        </div>
    </div>


<?php get_footer();