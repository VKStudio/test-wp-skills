<?php
/* Template Name: Adverts Page */

get_header();

// Meta key for custom meta box with image of post
$meta_key = 'imb_image';

// Default img for post output
$img_default = array(
    'img_src' => get_stylesheet_directory_uri() . '/images/no-img.png',
    'img_alt' => 'Without Photo'
);

// Get custom posts "ADVERTS"
$args = array(
    'post_type' => 'advert',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'orderby' => 'date',
    'order' => 'DECS',
);

$posts = new WP_Query($args);
?>
<div class="container">
    <div class="row row-cols-1 row-cols-md-4 cards-wrapper">
    <?php
        while ($posts->have_posts()) : $posts->the_post();
            $post_id = get_the_ID();
            $img = imb_attachment_image($post_id);
            $users_img = get_users_image($post_id, 'post_img_url');
            if (!$img['img_src']) {
                if ($users_img) {
                    $img = array(
                        'img_src' => $users_img,
                        'img_alt' => get_the_title()
                    );
                } else {
                    $img = $img_default;
                }
            }
            ?>
            <div class="col mb-3">
                <div class="card">
                    <img src="<?= $img['img_src']; ?>" class="card-img" alt="<?= $img['img_alt']; ?>">
                    <div class="card-body">
                        <h5 class="card-title"><?php the_title(); ?></h5>
                    </div>
                </div>
            </div>
            <?php
        endwhile;
    ?>
    </div>
</div>


<?php
wp_reset_postdata();


get_footer();