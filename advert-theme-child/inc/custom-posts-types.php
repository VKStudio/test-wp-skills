<?php

/* Custom post type for ADVERTS */
add_action('init', 'advert_posts_init');

function advert_posts_init()
{
    register_post_type('advert', array(
        'labels'             => array(
            'name'               => 'Объявления',
            'singular_name'      => 'Объявление',
            'add_new'            => 'Добавить новое',
            'add_new_item'       => 'Добавить новое объявление',
            'edit_item'          => 'Редактировать объявление',
            'new_item'           => 'Новое объявление',
            'view_item'          => 'Посмотреть объявление',
            'search_items'       => 'Найти объявление',
            'not_found'          => 'Объявлений не найдено',
            'not_found_in_trash' => 'В корзине объявлений не найдено',
            'parent_item_colon'  => '',
            'menu_name'          => 'Объявления'

        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 4,
        'supports'           => array('title', 'editor')
    ));
}
