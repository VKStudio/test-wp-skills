<?php

function imb_image_uploader_field( $name, $value = '', $users_img = '' ) {
    $image = 'Загрузить изображение';
    $image_size = 'full';
    $display = 'none';
    $image_attributes = wp_get_attachment_image_src( $value, $image_size );

    $response = array(
        'status' => true,
        'output' => ''
    );

    if ( $image_attributes ) {
        $image = '<img src="' . $image_attributes[0] . '" style="max-width:100%;display:block;" />';
        $display = 'inline-block';
    } else {
        $response['status'] = false;
        if ( $users_img ) {
            $image = '<img src="' . $users_img . '" style="max-width:100%;display:block;" />';
            $display = 'inline-block';
        }
    }

    $html = '<div>
                 <a href="#" class="imb_upload_image_button">' . $image . '</a>
                 <input type="hidden" name="' . $name . '" id="' . $name . '" value="' . $value . '" />
                 <a href="#" class="imb_remove_image_button" style="display:inline-block;display:' . $display . '">Удалить картинку</a>
             </div>';

    $response['output'] = $html;
    return $response;
}

// Add a meta box
add_action( 'admin_menu', 'imb_meta_box_add' );

function imb_meta_box_add() {
    add_meta_box(
        'imbdiv',
        'Картинка объявления',
        'imb_print_box',
        'advert',
        'side'
    );
}

// Meta Box HTML
function imb_print_box( $post ) {
    $meta_key = META_KEY_IMG;

    $image = imb_image_uploader_field( $meta_key, get_post_meta($post->ID, $meta_key, true) );

    if (!$image['status']) {
        $users_img = get_users_image( $post->ID, 'post_img_url' );
        $image = imb_image_uploader_field( $meta_key, '', $users_img );
    }

    echo $image['output'];
}

// Save Meta Box data
add_action('save_post', 'imb_save');

function imb_save( $post_id ) {
    $meta_key = META_KEY_IMG;

    update_post_meta( $post_id, $meta_key, $_POST[$meta_key] );

    return $post_id;
}

// Get attachment image of post for front-end
function imb_attachment_image( $post_id ) {
    $img_id = get_post_meta($post_id, META_KEY_IMG, true);
    $img_alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);

    $image_attributes = wp_get_attachment_image_src( $img_id, 'medium' );

    $img = array(
        'img_src'   => $image_attributes[0],
        'img_alt'   => $img_alt,
    );

    return $img;
}

// Get users image of post
function get_users_image( $post_id, $meta_key ) {
    if( !get_post_meta($post_id, $meta_key, true) ) return false;

    return get_post_meta($post_id, $meta_key, true);
}