<?php
// Form event from front-end - adding new ADVERT
add_action( 'wp_ajax_set_form', 'set_form' );
add_action( 'wp_ajax_nopriv_set_form', 'set_form');

function set_form() {
    check_ajax_referer( 'ajax-nonce', 'nonce_code' );

    $new_advert = array(
        'post_title'    => sanitize_text_field( $_POST['header'] ),
        'post_status'   => 'publish',
        'post_type'     => 'advert'
    );

    // To save the data in ADVERT Custom Post Type
    $post_id = wp_insert_post( $new_advert );

    // Validation attach files of form
    $arr_img_ext = array('image/png', 'image/jpeg', 'image/jpg');
    if (in_array($_FILES['file']['type'], $arr_img_ext)) {
        $upload = wp_upload_bits($_FILES["file"]["name"], null, file_get_contents($_FILES["file"]["tmp_name"]));
        $img_url = stristr($upload['url'], '/wp-content');
        update_post_meta( $post_id, 'post_img_url', $img_url );
    }

    // Task send email for WP Cron
    if(!wp_next_scheduled( 'send_emails_cron_event' )) {
        wp_schedule_single_event( time() + TIME_OUT_SEND_EMAIL, 'send_emails_cron_event', array( $_POST['email'], $_POST['header'] ) );
    }

    $admin_email = get_option('admin_email');
    $messages_for_admin = array(
        'new_post' => 'Добавлено новое объявление!'
    );

    wp_mail($admin_email, $_POST['header'], $messages_for_admin['new_post']);

    wp_die();
}

// Send mess to users email
add_action( 'send_emails_cron_event','do_send_email', 10, 2 );
function do_send_email( $email, $header ) {
    $messages = array(
        'thank_you'  => 'Ваше объявление было успешно добавлено! Спасибо'
    );

    wp_mail($email, $header , $messages['thank_you']);
}